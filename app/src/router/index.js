import Vue from 'vue'
import Router from 'vue-router'

const Home      = () => import('@/pages/Home')
const AboutMe   = () => import('@/pages/AboutMe')
const Projects  = () => import('@/pages/Projects')
const NotFound  = () => import('@/pages/404')

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/Home',
      name: 'Home',
      component: Home
    },
    {
      path: '/AboutMe',
      name: 'AboutMe',
      component: AboutMe
    },
    {
      path: '/Project/:key',
      name: 'Project',
      component: Projects
    },
    { path: '*', component: NotFound }
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      // savedPosition is only available for popstate navigations.
      return savedPosition
    } else {
      const position = {}
      if (to.hash) {
        position.selector = to.hash
        if (to.hash === '#projects') {
          position.offset = {
            y: 500
          }
        }
        if (document.querySelector(to.hash)) {
          return position
        }
        return false
      }
      return new Promise(resolve => {
        if (to.matched.some(m => m.meta.scrollToTop)) {
          position.x = 0
          position.y = 0
        }
        this.app.$root.$once('triggerScroll', () => {
          resolve(position)
        })
      })
    }
  }
})
