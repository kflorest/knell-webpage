import Vue from 'vue'
import Vuex from 'vuex'
import {HTTP} from '../http/http-common'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    menu : false,
    pages : [],
    projects : [
      {
        key: '1',
        title: 'Formalizar',
        description: 'Worpress, PHP, JS, Gulp, Sass, bower',
        image: 'home',
        color: 'blue',
        link: 'Formalizar',
        data: {
          client: 'Formalizar e-sginature',
          completed: 2017,
          tools: 'platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras',
          scope: 'Web design, Frontend Development'
        }
      },
      {
        key: '2',
        title: 'DelyFast',
        description: 'Nodejs, Angularjs, Sass, Gulp, Webpack',
        image: 'image1',
        color: 'green',
        link: 'DelyFast',
        data: {
          client: 'Delyfast',
          completed: 2017,
          tools: 'platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras',
          scope: 'Web design'
        }
      },
      {
        key: '3',
        title: 'kvell',
        description: 'Vuejs, Webpack, gulp, bower, bourbon',
        image: 'image2',
        color: 'orange',
        link: 'kvell',
        data: {
          client: 'Kvell Arnold',
          completed: 2017,
          tools: 'platea dictumst vestibulum rhoncus est pellentesque elit ullamcorper dignissim cras',
          scope: 'Web design, Frontend Development'
        }
      }
    ],
    skills : [
      {
        title: 'Development',
        list: [
          'Nodejs, C#',
          'Angular, React, Vue',
          'Php, Wordpress',
          'Sass',
          'Bower, Gulp, Grunt',
          'Webpack',
          'Android, Swift'
        ]
      },
      {
        title: 'Design',
        list: [
          'UX/UI Design',
          'Branding Design',
          'AI, PS, After Effects',
          'Sketch, InVision'
        ]
      },
      {
        title: 'Others Things',
        list: [
          'Speak English',
          'Speack Portugues',
          'Speack Spanish',
          'Cook very good',
          'Sea lover',
          'Draw nice things'
        ]
      }
    ]
  },
  actions: {
    FETCH_PAGES: function({commit, state}, $route) {

      var url = 'wp-json/wp/v2/pages/?orderby=menu_order&order=asc'

      HTTP.get(url)
      .then(response => {
        commit('SET_PAGES_LIST', { list: response.data })
      })
      .catch(e => {
        console.log(e)
      })
    },
    toggleMenu: ({commit}, obj) => {
      commit('TOGGLE_MENU', obj)
    },
    getProjectData: ({commit}, obj) => {
      commit('PROJECT_DATA', obj)
    }
  },
  mutations: {
    PROJECT_DATA: (state, obj ) => {
      state.currentProject = state.projects.filter((v) => {
        return v.link === obj
      })
    },
    TOGGLE_MENU: (state, obj ) => {
      state.menu = obj
    },
    SET_PAGES_LIST: (state, { list }) => {
      state.pages = list
    }
  },
  getters: {
    getMenuStatus:  state => state.menu,
    getProjects:    state => state.projects,
    getSkills:      state => state.skills,
    getProjectData: state => state.currentProject
  }
})
export default store